#FROM python:3.10
#RUN pip install pytest

#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

#ENV ASPNETCORE_URLS=http://+:5000

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
#WORKDIR /src
#COPY ["src/ValueShore.TestingApi/ValueShore.TestingApi.csproj", "src/ValueShore.TestingApi/"]
#RUN dotnet restore "src/ValueShore.TestingApi/ValueShore.TestingApi.csproj"
#COPY . .
#WORKDIR "/src/src/ValueShore.TestingApi"
#RUN dotnet build "ValueShore.TestingApi.csproj" -c Release -o /app/build

#FROM build AS publish
#RUN dotnet publish "ValueShore.TestingApi.csproj" -c Release -o /app/publish /p:UseAppHost=false

#FROM base AS final
#WORKDIR /app
#COPY --from=publish /app/publish .
#ENTRYPOINT ["dotnet", "ValueShore.TestingApi.dll"]
